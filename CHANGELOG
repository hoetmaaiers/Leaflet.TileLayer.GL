
# 2.4.1 (2020-06-04)

* Removed GeoTIFFs from distributed demos.

# 2.4.0 (2020-06-04)

* Added `uTileCoords` uniform
* Added the "pixelated" demo (using the new `uTileCoords` uniform to calculate an offset for the oversampling)

# 2.3.0 (2019-01-19)

* Added custom uniforms handling (`uniforms` option, `setUniform` and `reRender` methods)

# 2.2.0 (2019-01-15)

* Add a new `uNow` uniform, and a render loop. Animated shaders!

# 2.1.0 (2018-12-24)

* Add a new `tilelayers` option. Instead of just `tileUrls`, users can now provide instances of `L.TileLayer` (or even `L.TileLayer.WMS`). Doesn't break backwards compatibility (except if you're hot-reloading tile URLs).

# 2.0.0 (2017-05-05)

* Force using a predefined vertex shader, and force the varyings and uniforms in the fragment shader. This will make things simpler for most users. It also breaks backwards compat.
* Added a interactive REPL!

# 1.1.0

* Added a `aLatLngCoords` attribute
* Repurposed the `aCRSCoords` attribute: it now handles the tile's CRS coordinates, not its `LatLng` coordinates
* Added the "Tropical" demo

# 1.0.1

* Switch to subclassing from `GridLayer` instead of `TileLayer` to properly
  handle async draws and remove glitching.
* Prettified filenames and updated refs in `package.json`.
* Fixed CRS coords orientation *again* because GL coords systems are insane.
* Linting.

# 1.0.0

* Initial release
* Three demos (mandelbrot, terrain-rbg, antitoner)

